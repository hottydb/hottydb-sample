package org.example;

import java.sql.*;

public class RecommendEngine {
    public static void main(String[] args) throws SQLException {
        System.out.println("HottyDB: RecommendEngine Usage");

        /*===============
          URLによってリモートモードと組み込みモードの切り替えが可能.
          - リモートで起動中のHottyDBサーバに接続する場合
               String url = "jdbc:hottydb://127.0.0.1";

          - 組み込みモードでHottyDBを起動する場合
               String url = "jdbc:hottydb:tmp/test.db";
               (この例の場合、カレントディレクトリに tmp/test.dbというディレクトリが作成される)
        ===============*/
//        String url = "jdbc:hottydb://127.0.0.1"; // リモートモード
        String url = "jdbc:hottydb:tmp/recommend.db"; // 組み込みモード

        Connection connection = DriverManager.getConnection(url);
        connection.setAutoCommit(false);
        Statement statement = connection.createStatement();

        /*===============
         * 記事テーブルの作成
         *   - id: 記事ID
         *   - title: 記事タイトル
         *===============*/
        statement.executeUpdate("CREATE TABLE article (id INT, title VARCHAR(30))");

        /*===============
         * いいね管理テーブルの作成
         *   - uid: ユーザーID
         *   - aid: 記事ID
         *===============*/
        statement.executeUpdate("CREATE TABLE likes (uid INT, aid INT)");

        /*===============
         * いいねテーブルにレコメンドインデックスの作成
         *===============*/
        statement.executeUpdate("CREATE RECOMMEND INDEX r1 on likes (uid, aid)");

        /*===============
         * 記事テーブルにデータを挿入する
         *===============*/
        String insertF = "INSERT INTO article (id, title) VALUES (%d, '%s')";
        statement.executeUpdate(String.format(insertF, 1, "XXX社がYYY社と業務提携することになりました。"));
        statement.executeUpdate(String.format(insertF, 2, "XXX社はZZZ社が業務提携を解消"));
        statement.executeUpdate(String.format(insertF, 3, "XXX社はABC領域に業務を拡大することに"));
        statement.executeUpdate(String.format(insertF, 4, "ZZZの海外事業が順調です。"));
        statement.executeUpdate(String.format(insertF, 5, "ABCで有名なYYY。社長が交代します"));
        statement.executeUpdate(String.format(insertF, 6, "アイドルのHHHとサッカー選手のIIIの熱愛が発覚"));
        statement.executeUpdate(String.format(insertF, 7, "野球選手のIIIが三打数連続ホームラン"));
        statement.executeUpdate(String.format(insertF, 8, "アナウンサーのUUU、熱愛報道を否定。"));
        statement.executeUpdate(String.format(insertF, 9, "サッカー選手のJJJがハットトリック"));
        statement.executeUpdate(String.format(insertF, 10, "サッカー日本代表にJJJが選出されました"));

        /*===============
         * 記事テーブルを参照
         *===============*/
        ResultSet resultSet = statement.executeQuery(
                "SELECT id, title FROM article WHERE id = 2"
        );
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String title = resultSet.getString("title");
            System.out.printf("id=%d, title=%s \n", id, title);
        }
        resultSet.close();

        /*===============
         * いいねテーブルにデータを挿入する
         *===============*/
        insertF = "INSERT INTO likes (uid, aid) VALUES (%d, %d)";
        statement.executeUpdate(String.format(insertF, 1, 3));
        statement.executeUpdate(String.format(insertF, 1, 2));
        statement.executeUpdate(String.format(insertF, 1, 1));
        statement.executeUpdate(String.format(insertF, 2, 3));
        statement.executeUpdate(String.format(insertF, 2, 2));
        statement.executeUpdate(String.format(insertF, 2, 4));
        statement.executeUpdate(String.format(insertF, 3, 3));
        statement.executeUpdate(String.format(insertF, 3, 4));
        statement.executeUpdate(String.format(insertF, 3, 5));

        statement.executeUpdate(String.format(insertF, 4, 8));
        statement.executeUpdate(String.format(insertF, 4, 7));
        statement.executeUpdate(String.format(insertF, 4, 6));
        statement.executeUpdate(String.format(insertF, 5, 8));
        statement.executeUpdate(String.format(insertF, 5, 7));
        statement.executeUpdate(String.format(insertF, 5, 9));
        statement.executeUpdate(String.format(insertF, 6, 8));
        statement.executeUpdate(String.format(insertF, 6, 9));
        statement.executeUpdate(String.format(insertF, 6, 10));

        /*===============
         * いいねテーブルを参照
         *===============*/
        resultSet = statement.executeQuery(
                "SELECT uid, aid FROM likes WHERE aid = 2"
        );
        while (resultSet.next()) {
            int uid = resultSet.getInt("uid");
            int aid = resultSet.getInt("aid");
            System.out.printf("uid=%d, aid=%d \n", uid, aid);
        }
        resultSet.close();

        /*===============
         * 記事3にいいねしている人が、他にもいいねしている記事をレコメンドする
         *===============*/
        resultSet = statement.executeQuery(
                "SELECT r._item_id, r._similarity, a.title " +
                        "FROM recommend(r1, 3) r, article a " +
                        "WHERE r._item_id = a.id " +
                        "ORDER BY r._similarity DESC"
        );
        while (resultSet.next()) {
            int aid = resultSet.getInt("r._item_id");
            double similarity = resultSet.getDouble("r._similarity");
            String title = resultSet.getString("a.title");
            System.out.printf("id=%d, title=%s, similarity=%f \n", aid, title, similarity);
        }
        resultSet.close();

        /*===============
          テーブルとINDEXを削除
         ===============*/
        statement.executeUpdate("DROP TABLE article");
        statement.executeUpdate("DROP TABLE likes"); // RECOMMEND INDEXも自動削除される

        statement.close();
        connection.close();
    }
}
