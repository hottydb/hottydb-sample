package org.example;

import java.sql.*;

public class SearchEngine {
    public static void main(String[] args) throws SQLException, InterruptedException {
        System.out.println("HottyDB: SearchEngine Usage");

        /*===============
          URLによってリモートモードと組み込みモードの切り替えが可能.
          - リモートで起動中のHottyDBサーバに接続する場合
               String url = "jdbc:hottydb://127.0.0.1";

          - 組み込みモードでHottyDBを起動する場合
               String url = "jdbc:hottydb:tmp/test.db";
               (この例の場合、カレントディレクトリに tmp/test.dbというディレクトリが作成される)
        ===============*/
//        String url = "jdbc:hottydb://127.0.0.1"; // リモートモード
        String url = "jdbc:hottydb:tmp/search.db"; // 組み込みモード

        Connection connection = DriverManager.getConnection(url);
        connection.setAutoCommit(false);
        Statement statement = connection.createStatement();

        /*===============
         * 記事テーブルの作成
         *   - id: 記事ID
         *   - title: 記事タイトル
         *   - likes: 記事に対する「いいね」の数. その記事自体の人気度を示す.
         *===============*/
        statement.executeUpdate("CREATE TABLE article (id INT, title VARCHAR(30), likes INT)");

        /*===============
         * 全文検索を可能にするために、記事テーブルのタイトルに転置インデックスを作成
         *===============*/
        statement.executeUpdate("CREATE SEARCH INDEX s1 ON article (title)");

        /*===============
         * 記事テーブルにデータを挿入する
         *===============*/
        String insertF = "INSERT INTO article (id, title, likes) VALUES (%d, '%s', %d)";
        statement.executeUpdate(String.format(insertF, 1, "XXX社がYYY社と業務提携することになりました。", 100));
        statement.executeUpdate(String.format(insertF, 2, "XXX社はZZZ社が業務提携を解消", 80));
        statement.executeUpdate(String.format(insertF, 3, "XXX社はABC領域に業務を拡大することに", 30));
        statement.executeUpdate(String.format(insertF, 4, "ZZZの海外事業が順調です。", 3));
        statement.executeUpdate(String.format(insertF, 5, "ABCで有名なYYY。社長が交代します", 50));
        statement.executeUpdate(String.format(insertF, 6, "アイドルのHHHとサッカー選手のIIIの熱愛が発覚", 100));
        statement.executeUpdate(String.format(insertF, 7, "野球選手のIIIが三打数連続ホームラン", 90));
        statement.executeUpdate(String.format(insertF, 8, "アナウンサーのUUU、熱愛報道を否定。", 80));
        statement.executeUpdate(String.format(insertF, 9, "サッカー選手のJJJがハットトリック", 80));
        statement.executeUpdate(String.format(insertF, 10, "サッカー日本代表にJJJが選出されました", 10));

        /*===============
         * 単純な全文検索
         *===============*/
        // SEARCHメソッドにより全文検索が可能.
        // ORDER BYにより検索関連度でソートすることも可能.
        PreparedStatement preparedStatement = connection.prepareStatement(
                "SELECT id, title, likes, _similarity " +
                        "FROM SEARCH(article, title, ?) ORDER BY _similarity DESC");
        preparedStatement.setString(1, "業務 XXX");
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String title = resultSet.getString("title");
            int likes = resultSet.getInt("likes");
            double similarity = resultSet.getDouble("_similarity");
            System.out.printf("id=%d, title='%s', likes=%d, similarity=%f \n", id, title, likes, similarity);
        }
        resultSet.close();
        preparedStatement.close();

        /*===============
         * 機械学習ランキングの利用
         *===============*/
        // 特徴量テーブルのテンプレートを作成（特徴量とKEYをSELECTする必要がある）
        statement.executeUpdate("CREATE MLR_TEMPLATE t1 KEY(id) " +
                "SELECT id, title, likes, _similarity " +
                "FROM SEARCH(article, title, ?)");

        // 作成したテンプレートに対して、特徴量を指定し機械学習モデルを作成
        statement.executeUpdate("CREATE MLR_MODEL m1 WITH t1 (likes, _similarity)");

        // 検索と学習を繰り返す
        for (int i = 0; i < 7; i++) {
            // MLRメソッドで機械学習ランキングの推論実行
            resultSet = statement.executeQuery(
                    "SELECT _request_id, _key_id, title, _score, _similarity, likes FROM MLR(m1, '選手')"
            );
            int reqId = -1;
            while (resultSet.next()) {
                reqId = resultSet.getInt("_request_id");
                int id = resultSet.getInt("_key_id");
                int likes = resultSet.getInt("likes");
                String title = resultSet.getString("title");
                double score = resultSet.getDouble("_score");
                double similarity = resultSet.getDouble("_similarity");
                System.out.printf("reqId=%d, keyId=%d, title='%s', likes=%d, similarity=%f, score=%f \n",
                        reqId, id, title, likes, similarity, score);
            }
            resultSet.close();
            // INSERT MLR_POSITIVE命令で、リクエストIDとクリックされた記事IDを伝達し、機械学習ランキングの学習を実行.
            insertF = "INSERT MLR_POSITIVE(m1, %d, %d)";
            if (i <= 2)
                statement.executeUpdate(String.format(insertF, reqId, 9));
            else
                statement.executeUpdate(String.format(insertF, reqId, 6));
        }

        /*===============
         * PageView用メトリクステーブルを作成
         *  - id: 記事ID
         *  - num: 記事のPageView数
         *===============*/
        // ロックフリーなメトリクステーブルを作成
        statement.executeUpdate("CREATE METRICS TABLE page_view (id INT PRIMARY KEY, num INT) FLUSH_FREQ = 25");

        // PageViewを加算する
        preparedStatement = connection.prepareStatement(
                "ADD METRICS INTO page_view (id, num) VALUES (?, 1)");
        for (int i = 0; i < 100; i++) {
            preparedStatement.setInt(1, i % 10 + 1);
            preparedStatement.executeUpdate();
        }
        // 記事6, 7, 9だけ多めに加算する
        for (int i = 0; i < 100; i++) {
            if (i % 3 == 0)
                preparedStatement.setInt(1, 6);
            else if (i % 5 == 0)
                preparedStatement.setInt(1, 7);
            else
                preparedStatement.setInt(1, 9);
            preparedStatement.executeUpdate();
        }
        preparedStatement.close();

        // メトリクステーブルの中身を確認
        resultSet = statement.executeQuery("SELECT id, num FROM page_view");
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            int num = resultSet.getInt("num");
            System.out.printf("id=%d, num=%d \n", id, num);
        }
        resultSet.close();

        /*===============
         * メトリクステーブルを利用した機械学習ランキング
         *===============*/
        // 特徴量テーブルのテンプレートを作成（特徴量とKEYをSELECTする必要がある）
        statement.executeUpdate("CREATE MLR_TEMPLATE t2 KEY(id) " +
                "SELECT s.id AS id, title, likes, _similarity, num " +
                "FROM SEARCH(article, title, ?) AS s, page_view AS pv " +
                "WHERE s.id = pv.id");

        // 作成したテンプレートに対して、特徴量を指定し機械学習モデルを作成
        statement.executeUpdate("CREATE MLR_MODEL m2 WITH t2 (likes, _similarity, num)");

        // 検索と学習を繰り返す
        for (int i = 0; i < 7; i++) {
            // MLRメソッドで機械学習ランキングの推論実行
            resultSet = statement.executeQuery(
                    "SELECT _request_id, _key_id, title, _score, _similarity, likes, num FROM MLR(m2, '選手')"
            );
            int reqId = -1;
            while (resultSet.next()) {
                reqId = resultSet.getInt("_request_id");
                int id = resultSet.getInt("_key_id");
                int likes = resultSet.getInt("likes");
                int pv = resultSet.getInt("num");
                String title = resultSet.getString("title");
                double score = resultSet.getDouble("_score");
                double similarity = resultSet.getDouble("_similarity");
                System.out.printf("reqId=%d, keyId=%d, title='%s', likes=%d, similarity=%f, pv=%d, score=%f \n",
                        reqId, id, title, likes, similarity, pv, score);
            }
            resultSet.close();
            // INSERT MLR_POSITIVE命令で、リクエストIDとクリックされた記事IDを伝達し、機械学習ランキングの学習を実行.
            insertF = "INSERT MLR_POSITIVE(m2, %d, %d)";
            if (i <= 2)
                statement.executeUpdate(String.format(insertF, reqId, 9));
            else
                statement.executeUpdate(String.format(insertF, reqId, 6));
        }

        /*===============
          テーブルとINDEXを削除
         ===============*/
        statement.executeUpdate("DROP METRICS TABLE page_view");
        statement.executeUpdate("DROP MLR_MODEL m2");
        statement.executeUpdate("DROP MLR_TEMPLATE t2");
        statement.executeUpdate("DROP MLR_MODEL m1");
        statement.executeUpdate("DROP MLR_TEMPLATE t1");
        statement.executeUpdate("DROP TABLE article"); // 関連するSEARCH INDEXも自動削除

        statement.close();
        connection.close();
    }
}
