package org.example;

import java.sql.*;

public class StandardSQL {
    public static void main(String[] args) throws SQLException {
        System.out.println("HottyDB: StandardSQL Usage");

        /*===============
          URLによってリモートモードと組み込みモードの切り替えが可能.
          - リモートで起動中のHottyDBサーバに接続する場合
               String url = "jdbc:hottydb://127.0.0.1";

          - 組み込みモードでHottyDBを起動する場合
               String url = "jdbc:hottydb:tmp/test.db";
               (この例の場合、カレントディレクトリに tmp/test.dbというディレクトリが作成される)
         ===============*/
//        String url = "jdbc:hottydb://127.0.0.1"; // リモートモード
        String url = "jdbc:hottydb:tmp/standard.db"; // 組み込みモード

        Connection connection = DriverManager.getConnection(url);
        connection.setAutoCommit(false);
        Statement statement = connection.createStatement();

        /*===============
          テーブル作成
         ===============*/
        // 商品テーブルを作成
        statement.executeUpdate("CREATE TABLE product (id INT, name VARCHAR(20), category_id INT)");

        // カテゴリテーブルを作成
        statement.executeUpdate("CREATE TABLE category (id INT, name VARCHAR(20))");

        /*===============
          インデックス作成
         ===============*/
        // 商品テーブルのidフィールドにINDEXを作成
        statement.executeUpdate("CREATE INDEX i1 ON product (id)");

        /*===============
          データ挿入
         ===============*/
        // カテゴリテーブルにデータを挿入
        statement.executeUpdate("INSERT INTO category (id, name) VALUES (1, '冷蔵庫')");
        statement.executeUpdate("INSERT INTO category (id, name) VALUES (2, 'テレビ')");
        statement.executeUpdate("INSERT INTO category (id, name) VALUES (3, '洋服')");
        statement.executeUpdate("INSERT INTO category (id, name) VALUES (4, '本')");

        // 商品テーブルにデータを挿入
        PreparedStatement preparedStatement = connection.prepareStatement(
                "INSERT INTO product (id, name, category_id) VALUES (?, ?, ?)"
        );
        for (int i=1; i<=20; i++) { // TODO : Nを100にする
            preparedStatement.setInt(1, i); // id
            preparedStatement.setString(2, "すごい商品" + i); // name
            preparedStatement.setInt(3, (i%4+1)); // category_id
            preparedStatement.executeUpdate();
        }
        preparedStatement.close();

        /*===============
          データ更新
         ===============*/
        statement.executeUpdate("UPDATE product SET name = 'とてもすごい商品' WHERE id = 5");

        /*===============
          データ削除
         ===============*/
        statement.executeUpdate("DELETE FROM product WHERE id = 9");

        /*===============
          データ参照
         ===============*/
        // JOINによるデータ参照
        ResultSet resultSet = statement.executeQuery(
                "SELECT a.id, a.name, c.name " +
                        "FROM product AS a, category AS c " +
                        "WHERE a.category_id = c.id " +
                        "AND c.name = 'テレビ'"
        );
        while (resultSet.next()) {
            int a_id = resultSet.getInt("a.id");
            String a_name = resultSet.getString("a.name");
            String c_name = resultSet.getString("c.name");
            System.out.printf("a.id=%d, a.name='%s', c.name='%s' \n", a_id, a_name, c_name);
        }
        resultSet.close();

        // Group ByとOrder By
        resultSet = statement.executeQuery(
                "SELECT c.name, COUNT(a.id) count_of_category " +
                        "FROM product AS a, category AS c " +
                        "WHERE a.category_id = c.id " +
                        "GROUP BY c.name " +
                        "ORDER BY count_of_category DESC"
        );
        while (resultSet.next()) {
            String c_name = resultSet.getString("c.name");
            int count_of_category = resultSet.getInt("count_of_category");
            System.out.printf("a.name='%s', count=%d \n", c_name, count_of_category);
        }
        resultSet.close();

        /*===============
          テーブルとINDEXを削除
         ===============*/
        statement.executeUpdate("DROP TABLE product"); // 自動的に関連INDEXも削除される
        statement.executeUpdate("DROP TABLE category");

        statement.close();
        connection.close();
    }
}
