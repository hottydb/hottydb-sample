# HottyDBのサンプルプログラム

- [標準的なSQLの使い方](https://gitlab.com/hottydb/hottydb-sample/-/blob/main/src/main/java/org/example/StandardSQL.java)
- [検索エンジン機能の使い方](https://gitlab.com/hottydb/hottydb-sample/-/blob/main/src/main/java/org/example/SearchEngine.java)
- [レコメンドエンジン機能の使い方](https://gitlab.com/hottydb/hottydb-sample/-/blob/main/src/main/java/org/example/RecommendEngine.java)
